<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>PHP Test</title>
</head>
<body>
    <?php
	//DECLARATION DES VARIABLES
    $fichier = "biblio.txt";
	$livres = array();

	//APPEL DE FONCTIONS PRINCIPALES
	$livres = lireFichier($fichier);
	afficherTDM($livres);
	afficherTable($livres);


	/*  Function qui lit un fichier pass� selon le nom pass� en param�tre et
     *	retourne le tout sous forme d'un tableau 2 dimensions
     */
	function lireFichier($fichier){
		$monFichier = fopen($fichier, "r") or die("Fichier biblio.txt non disponible");

		while(!feof($monFichier)) {
			$entree = trim(fgets($monFichier));
			if ($entree == "* * *"){
				$livres[] = $ligne;
				unset($ligne);
			} else {
				$ligne[] = $entree;
			}
		}

		fclose($monFichier);
		return $livres;
	}

	/*  Fonction qui re�oit un tableau 2 dimensions de livres et cr�e une table des
     *	mati�res. Un lien est aussi cr�� vers le livre correspondant dans la table.
     */
	function afficherTDM($livres){

		printf("<H1>Table Des Mati�res</H1><UL>");
		foreach($livres as $livre){
			printf("<LI><A HREF=#" . $livre[0] . ">" . $livre[1] . "</A></LI>");
		}
		printf("</UL><BR><HR><BR>");
	}


	/* Fonction qui re�oit un tableau 2d de livres et l'affiche sous-forme de table
     */
	function afficherTable($livres) {
		$tableau = ["Identifiant:" , "Titre:" , "Auteurs:" ,
			"Ann�e de publication:" , "Maison d'�dition:" , "�dition:" ,
			"Site web de l'ouvrage:" , "Couverture"];

		foreach($livres as $livre){
			printf("<DIV ID=\"" . $livre[0] . "\"></DIV><TABLE width=100%% border=\"1\" >");
			foreach($livre as $index => $ligne){
				if ($ligne != ""){
					printf("<TR><TD width=25%%>" . $tableau[$index] . "</TD>");
					printf("<TD>" . getDonneesLivre($index, $ligne) . "</TD></TR>");
				}
			}
			printf("</TABLE><BR><BR>");

		}
	}

	/*Fonction interne qui retourne les donn�es du livre pass� en param�tre
     *
     * Entr�e:
     *	$index: L'index du livre � lire
    $ligne: Les donn�es du livre � l'index donn�
     */
	function getDonneesLivre($index, $ligne){
        $sortie = "";
        if ($ligne != ""){
            switch($index){
                case 6:
                    $sortie.= "<A HREF=\"" . $ligne . "\">" . $ligne . "</A>";
                    break;
                case 7:
                    $imageData = base64_encode(file_get_contents($ligne));
                    $sortie .= "<img src=\"data:image/jpeg;base64," . $imageData .
                        "\" alt=\"Image non disponible\">";
                    break;
                default:
                    $sortie .= $ligne;
					}
				}
		return $sortie;
	}
    ?>
</body>
</html>
